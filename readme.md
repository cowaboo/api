<p align="center"><img src="http://cowaboo.net/img/COWABOO_header_double.jpg"></p>

# CoWaBoo Api Sandbox Project

## About

This project is aiming at giving a space for students who want to play with the CoWaBoo Api 

## Installation

    $ git clone git@framagit.org:cowaboo/api.git cowaboo-api
    $ cd cowaboo-api
    $ composer update
    $ composer dumpautoload
    $ chmod 777 -R storage
    $ chmod 777 -R bootstrap/cache
    $ cp .env.example .env
    $ php artisan key:generate
